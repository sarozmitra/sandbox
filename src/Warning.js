import React from 'react';

const Warning = () => <span className={'warning'}>Take it easy!</span>;

Warning.displayname = 'Warning';

export default Warning;
